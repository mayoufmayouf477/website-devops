FROM httpd:latest
WORKDIR /usr/local/apache2/htdocs
COPY . .
RUN chmod -R 777 .
